# Jason Molding

JasonMould Industrial Company Limited, established in 2010, is a leading injection moulding manufacturer in China. JasonMould specializes in plastic mould production for household appliances, medical equipments, electronic equipments, safety equipments as well as monitoring system.

The company has over 15000 square metres factory. With plastic injection tooling divisions possessing different machinery and expertise, JasonMould offers tools from small and precise mobile phone parts to as big as automotive parts. Our tools are mainly exported to U.S.A., Europe, Japan and UK. JasonMould’s diversification strategy and full service has won compliment from customers all over the world.

Our mission is to provide our customers high quality moulding products and services. We aim to exceed our customers’ expectations of pricing, quality and on-time delivery. To achieve our mission, JasonMould:

1. Commits to continually improve employee skills and efficiency,
2. Provides our employees a clean and safe working environment,
3. Upgrades machine technology and support equipment through a strict maintenance program, and
4. Maintains customers’ tooling to its highest peak of performance

Address:LongGang Village, LongXi Town, BoLuo County,HuiZhou City, GuangDong Province, China

Email: info@jasonmolding.com

Web:  http://www.jasonmolding.com
